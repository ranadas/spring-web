package com.rdas.comps;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Created by ranadas on 21/01/2016.
 */

@Configuration
@PropertySource(value = { "classpath:application.properties" })
public class WebAppConfigurator {

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	@Bean
	public CheckDuplicateLogin getCheckDuplicateLogin() {
		return new CheckDuplicateLogin();
	}
}