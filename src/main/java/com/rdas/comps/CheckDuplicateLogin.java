package com.rdas.comps;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.apache.commons.collections.bag.HashBag;

import java.util.Calendar;

/**
 * Created by x148128 on 20/01/2016.
 */
@Slf4j
@Component
public class CheckDuplicateLogin {

	protected static HashBag hashbag = new HashBag();
	protected static Calendar bagResetTime = Calendar.getInstance();

	@Value("${login.bag.size}")
	private String loginBagSize;

	@Value("${login.max.limit}")
	private String loginMaxLimit;

	public void sayHello() {
		log.info("\n\n{}\n\n", "Hello");
	}
	public boolean checkDuplicates(String userid) {
		log.info("BagResetTime : " + bagResetTime.getTime());
		Calendar rightNow = Calendar.getInstance();
		// Subtract 1 hour from rightNow
		rightNow.add(Calendar.HOUR, -1);
		log.info("Right Now (minus 1 hour) is : " + rightNow.getTime());
		log.info("Is Right Now (minus 1 hour) greater than bagResetTime (+1 is true): " + rightNow.compareTo(bagResetTime));

		if (hashbag.size() > Integer.parseInt(loginBagSize) || (rightNow.compareTo(bagResetTime) > 0)) {
			log.info("Bag size is larger than" + loginBagSize + "or BagReset is past 1 hour; Clearing Bag & Reseting BagResetTime");
			hashbag.clear();
			bagResetTime = Calendar.getInstance();
		}
		hashbag.add(userid);
		if (hashbag.getCount(userid) > Integer.parseInt(loginMaxLimit)) {
			log.info("Bag contains : " + hashbag.getCount(userid) + " no of userids");
			return true;
		} else {
			log.info("Bag Size : " + hashbag.size());
			return false;
		}
	}
}