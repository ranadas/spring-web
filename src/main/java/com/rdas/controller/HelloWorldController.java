package com.rdas.controller;

import com.rdas.comps.CheckDuplicateLogin;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * Created by x148128 on 11/23/2015.
 */
@Slf4j
@Controller
public class HelloWorldController {

	@Autowired
	private CheckDuplicateLogin checkDuplicateLogin;

	@RequestMapping("/")
	public String helloWorld(Model model) {
		model.addAttribute("message", "Hello World!");
		return "index";
	}

	@RequestMapping("/1")
	public String sayHelloWorld(Model model) {
		model.addAttribute("message", "Hello World! from spring 1");
		return "first";
	}

	@RequestMapping("/index")
	public String home() {
		log.info("\n ###### Servicing home\n");
		return "index";
	}

	@RequestMapping("/welcome")
	public ModelAndView helloWorld() {
		log.info("\n ###### servicing welcome\n");
		String message = "<br><div style='text-align:center;'>"
				+ "<h3>********** Hello World, Spring MVC Tutorial</h3>" +
				"This message is coming from HelloWorldController.java **********</div><br><br>";
		return new ModelAndView("welcome", "message", message);
	}
}