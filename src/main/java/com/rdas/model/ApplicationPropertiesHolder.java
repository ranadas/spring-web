package com.rdas.model;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * Created by x148128 on 21/01/2016.
 */
@Slf4j
@Component
public class ApplicationPropertiesHolder implements InitializingBean {
	@Override
	public void afterPropertiesSet() throws Exception {
		log.debug("\n\n{}\n", "afterPropertiesSet");
	}
}