A simple Spring-web mvc app. 
With a filter that allows only GET & POST. A bean is @Autowired in the filter. 

1. Add the snippet as marker in web.xml
2. in Filter init () method add the code to enable autowire of the bean. Only then Autowire.  
If intellij source folder is corrupted, then use the following. 

      <sourceFolder url="file://$MODULE_DIR$/src/main/java" isTestSource="false" />
      <sourceFolder url="file://$MODULE_DIR$/src/main/resources" type="java-resource" />
      <sourceFolder url="file://$MODULE_DIR$/src/test/java" isTestSource="true" />
      <excludeFolder url="file://$MODULE_DIR$/target" />